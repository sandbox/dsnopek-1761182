<?php

function webform_answers_delete($nid) {
  db_query("DELETE FROM {webform_answers} WHERE nid = %d", $nid);
}

function webform_answers_create($nid, $answers) {
  foreach ($answers as $cid => $values) {
    foreach ($values['value'] as $delta => $value) {
      $data = array(
        'nid'  => $nid,
        'cid'  => $cid,
        'no'   => $delta,
        'data' => is_null($value) ? '' : $value,
      );
      drupal_write_record('webform_answers', $data);
    }
  }
}

function webform_answers_update($nid, $answers) {
  webform_answers_delete($nid);
  webform_answers_create($nid, $answers);
}

function webform_answers_load($nid) {
  $answers = array();

  $res = db_query("SELECT * FROM {webform_answers} WHERE nid = %d", $nid);
  while ($obj = db_fetch_object($res)) {
    if (!isset($answers[$obj->cid])) {
      $answers[$obj->cid] = array('value' => array());
    }
    $answers[$obj->cid]['value'][$obj->no] = $obj->data;
  }

  return $answers;
}

