<?php

/**
 * @file
 * Manages anwsers on a webform
 */

/**
 * The form to set the answers on.
 */
function webform_answers_form(&$form_state, $node) {
  module_load_include('inc', 'webform_answers', 'webform_answers.crud');

  // TODO: see notes in webform_answers.install - not sure how to fill in defaults but this
  // should actually start with the tree data. Maybe we flatten it here?

  $answers = webform_answers_load($node->nid);
  $submission = !empty($answers) ? (object)array('data' => $answers) : NULL;

  // remove any components that aren't marked as having answers
  foreach ($node->webform['components'] as $cid => $component) {
    if (empty($component['extra']['webform_answers'])) {
      unset($node->webform['components'][$cid]);
    }
  }

  // TODO: should we run it in a way that the form alter hooks will happen?
  $form = webform_client_form($form_state, $node, $submission);

  // avoid a chicken and the egg situation!
  //$form['#validate'] = array_diff($form['#submit'], array('webform_answers_validate'));

  // we don't want to store the submission or send any e-mails
  $form['#submit'] = array_diff($form['#submit'], array('webform_client_form_submit'));

  // call our submit function
  $form['#submit'][] = 'webform_answers_form_submit';

  return $form;
}

/**
 * Store the answers.
 */
function webform_answers_form_submit($form, &$form_state) {
  module_load_include('inc', 'webform', 'includes/webform.submissions');
  module_load_include('inc', 'webform_answers', 'webform_answers.crud');

  $node = $form['#node'];

  // TODO: see notes in webform_answers.install - this should actually store the
  // submitted_tree values.

  $answers = webform_submission_data($node, $form_state['values']['submitted']);
  webform_answers_update($node->nid, $answers);

  drupal_set_message(t('Answers updated.'));
}

